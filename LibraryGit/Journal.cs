﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryGit
{
    class Journal
    {
        /// <summary>
        /// Словарь в котором храниться инфо кто и на сколько взял книгу.
        /// </summary>
        public Dictionary<Book, string[]> byBook = new Dictionary<Book, string[]>();

        /// <summary>
        /// Взять книгу из библиотеки.
        /// </summary>
        public void TakeBook(Book book, string userName, int daysCount)
        {
            if (book != null) // Условие на существование книги.
            {
                byBook.Add(book, new string[] { userName, Convert.ToString(daysCount) });
                book.DateTimeTaking = DateTime.Now; // Ставим дату взятия книги текущим временем.
                book.DaysAbsence += daysCount; // В книгу добавляем дни ее теоритического отсутствия(Пользователь пришел и сказал, что взял на 10 дней)
            }
        }

        /// <summary>
        /// Возрат книги.
        /// </summary>
        public void ReturnBook(Book book)
        {
            byBook.Remove(book);
            book.RealDaysAbsence += DateTime.Now - book.DateTimeTaking; // Инфо сколько книги небыло в библитеке.(Общее время сколько небыло)
            Console.WriteLine("You have successfully returned the book to the library");
        }
    }
}
