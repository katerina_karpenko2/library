﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryGit
{
    class Menu
    {
        public static void PrintListOfOptions()
        {
            Console.WriteLine("Welcome to the library. Choose what you want to do: \n"
                + "1. View a list of books in the library\n"
                + "2. Add book to catalog\n"
                + "3. Remove book from catalog\n"
                + "4. List of books in alphabetical order\n"
                + "5. Show books by author\n"
                + "6. Find a book by one or several words\n"
                + "7. Take a book to read\n"
                + "8. Return a book\n"
                + "9. Look to statistic of using each book\n"
                + "10. Exit\n");
        }

        /// <summary>
        /// Просьба выбора сортировка книги.
        /// </summary>
        public static string AskForSortingParametr()
        {
            bool invalidValue = false;
            string userEnter;
            do
            {
                Console.WriteLine("Select the sorting option of library, please: \n"
                    + "1. Author\n"
                    + "2. Title\n");
                userEnter = Console.ReadLine();
                invalidValue = userEnter != "1" && userEnter != "2";
                if (invalidValue)
                {
                    Console.WriteLine("Please, enter valid number");
                }
            }
            while (invalidValue);
            return userEnter;
        }

        /// <summary>
        /// Регистрация.
        /// </summary>
        public static string AskName()
        {
            Console.WriteLine("Hi. Let's register you. First enter your name, please:");
            return Console.ReadLine();
        }

        /// <summary>
        /// Колличество дней желаемое юзером.
        /// </summary>
        public static int AskForNumberOfDays()
        {
            int daysCount = 0;
            bool isInvalidValue = false;
            do
            {
                Console.WriteLine("For how many days you want to take a book to read?\n Number of days can be from 1 to 30.");
                int.TryParse(Console.ReadLine(), out daysCount);
                isInvalidValue = daysCount == 0 || daysCount > 30; // Проверка на валидность данных
                if (isInvalidValue)
                {
                    Console.WriteLine("Error, please enter valid number");
                }
            }
            while (isInvalidValue); // Спрашиваем до того времени пока не введут хотя-бы 1 день
            return daysCount;
        }

        /// <summary>
        /// Поиск по автору.
        /// </summary>
        public static string AskForAuthor()
        {
            Console.WriteLine("Please, enter book's author: ");
            return Console.ReadLine();
        }

        /// <summary>
        /// Просьба ввести по автору и названию.
        /// </summary>
        public static string[] AskAuthorAndTitle() // [] - только массив вернет 2 строковых значения.
        {
            string[] authorAndTitle = new string[2];
            authorAndTitle[0] = AskForAuthor();
            Console.WriteLine("Please, enter book's title: ");
            authorAndTitle[1] = Console.ReadLine();
            return authorAndTitle;
        }

        /// <summary>
        /// Поиск по тексту.
        /// </summary>
        public static string AskSearchText()
        {
            Console.WriteLine("Enter text to search: ");
            return Console.ReadLine();
        }

        /// <summary>
        /// Поиск книги для взятия из библиотеки.
        /// </summary>
        public static string[] AskForBookToRead()
        {
            Console.WriteLine("Let's find the book you want to read.");
            return AskAuthorAndTitle();
        }

        public static string[] AskForBookToReturn()
        {
            Console.WriteLine("Let's clarify which book you want to return.");
            return AskAuthorAndTitle();
        }
    }
}
