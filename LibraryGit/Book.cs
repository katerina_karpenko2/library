﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryGit
{
    class Book
    {
        public string Author;
        public string Title;
        public int DaysAbsence; // Длительность взятия книги, всего по записи времни(Юзер взял на 10 дней, вернул через 3 дня, по записи осталось 10, в фактическую зависало 3)
        public TimeSpan RealDaysAbsence; // Сколько дней фактически она отсутствовала.
        public DateTime DateTimeTaking; // Дата взятия книги.
        public bool Availability = true;

        public Book(string author, string title)
        {
            Author = author;
            Title = title;
        }
    }
}
