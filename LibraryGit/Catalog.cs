﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryGit
{
    class Catalog
    {/// <summary>
     /// Список книг библиотеки.
     /// </summary>
        public List<Book> ListOfBook = new List<Book>
        {
            new Book("King", "Black Tower"),
            new Book("Lovecraft", "The Call Of Cthulhu and Other Weird Stories"),
            new Book("Shekespeare", "Hamlet"),
            new Book("Gogol", "Dead Souls"),
            new Book("Bulgakov", "The Master and Margarita"),
        };

        /// <summary>
        /// Вывод списка книг в библиотеке.
        /// </summary>
        public void ShowListOfBooks(List<Book> books)
        {
            Console.WriteLine("Dear visitor, there are books in the library: ");
            int count = 1;
            foreach (Book book in books)
            {
                Console.WriteLine($"{count}. {book.Author} {book.Title}");
                count++;
            }
        }

        /// <summary>
        /// Добавление в библиотеку книг пользователем.
        /// </summary>
        public void AddBookToList(string author, string title)
        {
            if (!IsBookExistInList(author, title)) // Условие отрабатывает только если книги нет.
            {
                ListOfBook.Add(new Book(author, title));
                Console.WriteLine("New book added to the library");
            }
            else
            {
                Console.WriteLine("This book is already in the library");
            }
        }
        /// <summary>
        /// Удаление из библиотеки книг пользователем.
        /// </summary>
        public void RemoveBookFromList(string author, string title)
        {
            if (IsBookExistInList(author, title))
            {
                ListOfBook.Remove(new Book(author, title));
                Console.WriteLine("Book removed from library.");
            }
            else
            {
                Console.WriteLine("There is no such book in the library!");
            }

        }

        /// <summary>
        /// Сортировка через LINQ.
        /// </summary>
        public List<Book> GetSortedListOfBooksByAutor()
        {
            IOrderedEnumerable<Book> tempList = from book in ListOfBook
                                                orderby book.Author
                                                select book;
            return tempList.ToList();
        }

        public List<Book> GetSortedListOfBooksByTitle()
        {
            IOrderedEnumerable<Book> tempList = from book in ListOfBook
                                                orderby book.Title
                                                select book;
            return tempList.ToList();
        }

        /// <summary>
        /// Метод проверки наличие книги в библиотеке.
        /// </summary>
        private bool IsBookExistInList(string author, string title)
        {
            bool isBookExist = false;
            foreach (Book book in ListOfBook) // Проверка каждой книги в листе на соответствию автора и названия.
            {
                isBookExist = book.Author.Equals(author) && book.Title.Equals(title);
                if (isBookExist)
                {
                    break;
                }
            }
            return isBookExist;
        }

        /// <summary>
        /// Поиск по автору.
        /// </summary>
        public void ShowBooksByAuthor(string author)
        {
            string listOfBooks = $"List of books writen by {author}: ";
            int count = 1;
            foreach (Book book in ListOfBook) // Поиск по листу определенного автора.
            {
                if (book.Author.ToLower().Equals(author.ToLower())) // Совпадение по введеному автору и наличию.
                {
                    listOfBooks += $"\n{count}. {book.Title}";
                    count++; // Увеличивает если есть хоть одна
                }
            }
            if (count == 1)
            {
                Console.WriteLine("Sorry, but there is no one book of this author.");
            }
            else
            {
                Console.WriteLine(listOfBooks);
            }
        }

        /// <summary>
        ///  Поиск книги по слову или словам в тексте.
        /// </summary>
        public void FindBookByTitle(string searchText)
        {
            int count = 1;
            string foundBooks = "Books found by your request: ";
            foreach (Book book in ListOfBook) // В переменную book по очереди ложим книги из листа, переменная book содержит єкзепляр класса книги и принадлежащее ей
            {
                if (book.Title.ToLower().Contains(searchText.ToLower()))
                {
                    foundBooks += $"\n{count}. {book.Author} {book.Title}";
                    count++;
                }
                if (count == 1)
                {
                    Console.WriteLine("Sorry. No books found for your request.");
                }
            }
        }

        /// <summary>
        /// Отдает книгу по названию и автору.
        /// </summary>
        public Book GetBookByAuthorAndTitle(string author, string title)
        {
            Book result = null;
            if (IsBookExistInList(author, title))
            {
                foreach (Book book in ListOfBook) // Проверка каждой книги в листе на соответствию автора и названия.
                {
                    if (book.Author.Equals(author) && book.Title.Equals(title))
                    {
                        result = book;
                        break;
                    }
                }
            }
            else
            {
                Console.WriteLine("There is no such book in the library");
            }
            return result;
        }

        /// <summary>
        /// Вывод статискики по юзенгу книги.
        /// </summary>
        public void ShowUsingStatistic()
        {
            Console.WriteLine("Books usage statistics at the library:");

            int count = 1;
            foreach (Book book in ListOfBook)
            {
                Console.WriteLine($"\n{count}. {book.Author} {book.Title}"
                    + $"\nReaders should have had the book {book.DaysAbsence}"
                    + $"\nBook was absent in the library for {book.RealDaysAbsence.Days}");
                count++;
            }
        }
    }
}
